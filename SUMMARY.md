# Summary

## Atelier - Actualisation des connaissances

* [Avant-propos et jeu de données](/actu.md#avant-propos-cest-quoi-un-sig-)
* [1. Paramétrage de QGIS](/actu.md#1-paramétrage-de-qgis)
* [2. Ajouter des données au SIG](/actu.md#2-ajouter-des-données-au-sig)
* [3. Numérisation et accrochage](/actu.md#3-numérisation-et-accrochage)
* [4. Manipulation de données attributaires](/actu.md#4-manipulation-de-données-attributaires)
* [5. Représentation et interrogation des données](/actu.md#5-repr%C3%A9sentation-et-interrogation-des-donn%C3%A9es)
* [6. Cartographier les données](/actu.md#6-cartographier-les-données)