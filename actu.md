# ![logo](images/sigActu.png) Atelier "Actualisation des Connaissances"

{% hint style='tip' %}
Les manipulations et captures d'écran de cette fiche ont été réalisées sur la version 3.10 de QGIS
{% endhint %}

**Objectifs** : reprendre tous les éléments abordés lors de la formation SIG Niveau 1 (Module découverte)

1. Paramétrage général du projet
2. Alimentation du SIG : ajout de données
3. Numérisation et accrochage
4. Manipulation de données attributaires
5. Représentation & interrogation de données
6. Edition de cartes



# Avant-propos : c'est quoi un SIG ?

Un système d'information géographique permet de manipuler des objets qui ont une représentation graphique dans un espace géographique défini (RGF93 - Lambert 93 par exemple), associée à des attributs. Un attribut est une information qui décrit l'objet selon des caractéristiques définies : identifiant, longueur, largeur, nature, etc.)

Les SIG permettent donc la conception et l'utilisation de bases de données relationnelles spatiales. On passe ainsi de l'illustration au traitement de données : dès lors qu'on "dessine" dans un SIG, on crée de la donnée potentiellement exploitable à partir de constats et d'un raisonnement scientifique. 

## Le jeu de données

Nous allons travailler sur les fichiers liés à un site archéologique situé en Bretagne, au lieu-dit "Kersulec". Afin de rappeler à chacun les bonnes pratiques, les données sont organisées dans l'arborescence-type des NAS.

![arborescence des NAS](images/image_001.png) 

# 1. Paramétrage de QGIS

## Le SCR

Avant toute chose, il est nécessaire de choisir le système de projection dans lequel on va travailler. 

Le choix du système de projection par défaut se fait dans le menu *Préférences > Options > SCR* : choisir **EPSG 2154 : Lambert 93 IGNF 93**

![choisir le système de coordonnées de référence](images/image_002.png)

## Les extensions

Vérifier que l'extension *Géoréférenceur GDAL* est bien activée. Sélectionner et installer d'autres extensions utiles : 

* Bezier Editing
* Maps Printing
* Vérificateur de géométrie
* Vérificateur de topologie



## Le profil utilisateur

La création d'un profil utilisateur permet à QGIS de mémoriser vos paramètres : les extensions téléchargées, les modèles créés mais aussi les barres d'outils activées et leur emplacement. 

Si vous utilisez un ordinateur partagé, l'activation de ce profil vous permet de vous sentir "chez vous" à chaque fois que vous ouvrez QGIS. 

L'ensemble des paramètres est mémorisé dans le dossier QGIS 3.



## Emplacement du dossier QGIS 3

Il peut être trouvé à l'adresse suivante :

```
C:\Users\VotreSession\AppData\Roaming\QGIS\QGIS3\profiles
```

Si vous n'avez pas créé de profil utilisateur, l'adresse complète est : 

```
C:\Users\VotreSession\AppData\Roaming\QGIS\QGIS3\profiles\default
```

Si vous avez créé un profil utilisateur avec votre prénom par exemple, au lieu de choisir le dossier *default*, il faut choisir celui avec votre prénom.

**Attention** L'accès au dossier *AppData* nécessite que les dossiers cachés soient visibles. Pour cela, allez dans votre explorateur, *Affichage > Options > Options des dossiers* et cochez la case "Afficher les fichiers et les dossiers cachés".

![afficher les dossiers cachés](images/image_003.png)



# 2. Ajouter des données au SIG

Le rangement des données est une composante importante du SIG. Une règle à retenir : dans l'arborescence, le projet QGIS (extension.qgz) doit être localisé "au-dessus" des fichiers qu'il contient. Une [arborescence normalisée des NAS](/actu.md#le-jeu-de-données) et des dossiers d'opération a été établie par la DST et la DSI afin de faciliter l'archivage des données. Il est fortement conseillé de suivre cette arborescence. 

Les données utilisées dans un SIG peuvent être de type **vecteur, raster ou tableau**. Les vecteurs et les rasters peuvent être associés à une information géographique, auquel cas on parle de données spatiales. 



## étape 1 : Ouvrir un projet vierge et l'enregistrer

Enregistrer le projet dans F111688_Kersulec : il s'appellera "**F111688_Kersulec_PlanFouille.qgz**"

## étape 2 : Constituer votre SIG grâce aux fonds de carte disponibles

* Importer les vecteurs **Cadastre.shp** et **Courbes_de_niveau.shp** (*donnee_spatiale > vecteur_georef > vecteur_fonds_carto*) et zoomer sur l’emprise de **Courbes_de_niveau**

* Charger un flux WMS à partir du fichier .xml

  :bulb: [fiche technique : Ajouter un flux WMS](https://formationsig.gitlab.io/fiches-techniques/raster/01_flux_WMS.html)

* Importer les flux WMS (IGN-Raster) **201-BdOrtho** et (IGN-Raster) **273-Scan25Topo** et les enregistrer comme image à une échelle permettant de voir l’ensemble des courbes de niveau ; ex : BdOrtho_2000e.jpg et Scan25_4000e.jpg (dans *> donnee_spatiale_image_georef*)



Pour créer une capture d'écran géoréférencée : Menu *Projet > Importer/Exporter > Exporter la carte au format Image*

* Importer ces deux rasters et supprimer du projet les flux. Créer un groupe « **Fonds_Carto** » pour ces couches, y faire des sous-groupes « **Rasters** » et « **Vecteurs** »
* Modifier rapidement les couleurs des couches vecteur pour faire joli

Pour modifier l'apparence d'une couche vecteur : clic droit sur la couche vecteur *> Propriétés > Symbologie*. Vous pouvez à partir de ce panneau modifier le contour et le remplissage, les couleurs, les épaisseurs de ligne, etc. Penser à sélectionner "Remplissage simple" pour avoir accès à l'ensemble des paramètres.



![panneau symbologie dans les propriétés](images/image_004.png)



# Géoréférencer des images

## étape 3 : Alimenter le SIG grâce au géoréférencement

:bulb: [fiche technique : Géoréférencer une image](https://formationsig.gitlab.io/fiches-techniques/raster/02_georef.html)

* Importer les autres vecteurs de l’opération (*donnee_spatiale > vecteur_georef*)

* Géoréférencer le raster Minute8_F77_F78.jpg sur F77 (*3_Donnees_Terrain > Plans*)

  


# 3. Numérisation et accrochage

## étape 4 : Dessiner les vestiges 77 et 78

:bulb: ​ fiche technique : [dessiner dans QGIS : les outils de base](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/01_numerisation_1.html)

* Avec l’outil *Ajouter une entité*

![ajouter une entité polygone](images/image_005.png) ajouter une entité dans une couche de polygones

![ajouter une entité ligne](images/image_006.png) Ajouter une entité dans une couche de lignes

![ajouter une entité point](images/image_007.png)Ajouter une entité dans une couche de points



Pour les lignes et les polygones, cliquer jusqu'à obtenir le tracé désiré, puis clic droit pour fermer la forme et ouvrir le formulaire (pour remplir la table attributaire)



* Avec l’outil *Bezier Editing*

![barre d'outils Bezier Editing](images/image_008.jpg) 

Création d’un dessin avec des points d’ancrage. La fermeture de la forme se fait avec un clic droit comme pour l'ajout d'une entité.

Cliquer sur œil pour afficher les points d'ancrage et les poignées. 

**Attention**, les modifications de points d’ancrage et de poignées ne peuvent être faites qu’avant la fermeture de la forme !



## étape 5 : Corriger les vestiges 10 et 116

:bulb: ​ [fiche technique : Accrochage et topologie](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/02_accrochage.html)

* Activer et paramétrer l’accrochage sur **emprise**
* Avec l’outil de nœud, corriger le fossé 116 pour qu’il rejoigne l’emprise

:bulb: ​ [fiche technique : Outil de noeuds](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/03_outil_noeuds.html)



## étape 6 : Fusionner des entités

* Fusionner les fossés 19 et 20

* Fusionner les fossés 18 et 21

![outil fusionner des entités](images/image_009.png) La fusion se fait avec l'outil *Fusionner les entités* de la barre d'outils de numérisation. Il est possible de choisir les champs qui seront retenus à l'issue de la fusion.

![choix des champs à fusionner](images/image_010.png) 




## étape 7 : Vérifier les géométries
:bulb: ​ [fiche technique : vérification de validité et géométrie](https://formationsig.gitlab.io/fiches-techniques/vecteur_dessin/04_verif_geom.html)

A toutes les étapes de la numérisation (lever topographique, dessin), des erreurs de géométrie sont susceptibles de se glisser dans vos vecteurs. Ces erreurs peuvent être des nœuds en double, des segments qui s'entrecroisent. 

Il est important de les corriger, car la présence d'erreurs de géométrie dans les couches vecteurs empêche certaines manipulations, comme les jointures spatiales. Dans certains cas, elles peuvent même faire planter le logiciel. 

* *Vecteur > Outils de géométrie > Vérifier la validité*

![outil Vérifier la validité](images/image_011.png)  

L'outil Vérifier la validité va créer trois couches temporaires : sortie valide, sortie invalide et erreur de sortie. 

![trois couches temporaires](images/image_012.png) 

Pour procéder aux corrections, ouvrir la table attributaire de la couche *Erreur de sortie* : elle recense les erreurs (nœuds en doublon, auto-intersection de segments). En sélectionnant la ligne de l'erreur et en zoomant sur l'entité sélectionnée (Ctrl + J), il est possible de voir l'erreur puis de la corriger. 

Recommencer pour chacune des lignes. A la fin des corrections, supprimer ces trois champs temporaires et recommencer le traitement pour vérifier que toutes les erreurs ont bien été corrigées. 

* *Vecteur > Vérifier les géométries*

Cet outil permet de vérifier la géométrie et la topologie. 

![outil Vérifier les géométries](images/image_013.png) 



En bas de la fenêtre, il est possible de choisir entre *Modifier les couches source* et *Créer une nouvelle couche*. Il est préférable de choisir la seconde option. 

Cliquer sur *Exécuter*. 

Le second onglet (Résultat) de la fenêtre s'affiche : 

![onglet Résultat de l'outil Vérifier les géométries](images/image_015.png) 

Cet outil permet de corriger les erreurs selon une correction par défaut (par exemple pour les nœuds en double, suppression du doublon) ou bien selon une correction choisie par l'utilisateur.



# 4. Manipulation de données attributaires

## étape 8 :  Alimenter le SIG avec les levés TOPO 

* Importer les vecteurs du dernier levé topo du 02/06/2016 (*donnee_spatiale > topographie > topo_vecteur* : prendre les 4 vecteurs). Grouper dans « 20160602 » 
* Vérifier l’encodage de TOUTES les couches vecteur 

![encodage de la couche vecteur](images/image_016.png) 



:bulb: ​ [fiche technique : calculatrice de champs](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/01_calcu_champ.html)

* Créer un champ « date » par couche vecteur topo en y appliquant la valeur ‘2016-06-02’ 

![création d'un champ dans la table attributaire via la calculatrice de champ](images/image_017.png) 

* Vérifier les tables attributaires et la géométrie
* Créer une couche de polygone F111688_NonAccess (avec un champ « interpret » de format texte longueur 10 ) *Créer une couche > Nouvelle couche shapefile*

![création d'une couche shapefile](images/image_018.png) 

* Copier-Coller les entités du levé topo dans leurs couches « opération » respectives (ex : Copier F111688_poly_20160527 et les coller dans F111688_poly)



## étape 9 :  Alimenter le SIG avec les inventaires de terrain 

:bulb: ​ [fiche technique : Jointure attributaire](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/02_jointure_att.html)

* Faire une jointure attributaire entre F111688_poly et le tableur d’inventaire des faits  (*etude > etude_RO > F111688_fait.csv*). Pérenniser la jointure (Exporter) en F111688_poly_inv.shp (dans *donnee_spatiale > vecteur_georef > vecteur_analyse*) **attention :** vérifier l'encodage du tableur avant de faire la jointure !

* Nettoyer SHP F111688_poly_inv à l’aide de la calculatrice de champs (mise à jour du champ ‘‘interpret’’ avec une sélection préalable des entités pour lesquelles le champ "type_fait" a une valeur renseignée) et supprimer les champs en trop (‘‘_type_fait’’) 

![sélection par expression](images/image_019.png) 



![mise à jour d'un champ par rapport avec les données d'un autre champ](images/image_020.png) 

* De la même manière, remplir le champ _chrono à partir des données "datedebut" et "datefin"
  * -2200 à -1600 : Age du Bronze
  * -50 à 235 : Antiquité
  * 1789 à nos jours : Contemporain



## Étape 10 : Alimenter le SIG via la jointure spatiale 

:bulb: ​ [fiche technique : Jointure spatiale](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/03_jointure_spatiale.html)

* Faire une jointure spatiale : *Traitement – Joindre les attributs par localisation (résumé)* pour récupérer les altitudes (couche pts_Topo) sur la couche _axe. 
  * Couche source : F111688_axe
  * Joindre la couche : F111688_pts_Topo
  * Prédicat géométrique : "intersecte"
  * Champs à résumer : ne sélectionner que le champ avec l'information d'altitude 
  * Résumés à calculer : ici, ne sélectionner que la moyenne
  * **Attention ! **La couche obtenue est *temporaire* "Couche issue de la jointure spatiale" 

![joindre les attributs par localisation](images/image_021.png) 

* *Vecteur – Outils de gestion de données – Joindre les attributs par localisation*. Objectif : remplacer le numéro d'axe par le numéro du fait coupé
  * Couche source : F111688_axe
  * Joindre la couche : F111688_poly_inv
  * Champ à ajouter : numpoly
  * Type de jointure : prendre uniquement les attributs de la première entité localisée

![joindre les attributs par localisation](images/image_022.png) 

* Mettre à jour le champ "numaxe"



# 5. Représentation et interrogation des données

:bulb: ​ [fiche technique : les opérateurs SQL](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/08_operateurs_sql.html)

## étape 11 : Catégoriser et représenter les données

* Catégoriser les vestiges selon le type selon le type de fait

  * *Propriétés > Symbologie > Catégorisé > Valeur* : "_type_fait"
  * Ne conserver que les champs fosse, fossé, fossé parcellaire, four, puisard et TP
  * Masquer le champ "annulé" et basculer les autres dans "toutes les autres valeurs"
  * Choisir un contour noir et pas de remplissage pour "toutes les autres valeurs"
  * **attention** certains champs de la table attributaire sont certainement non renseignés, à vous de les corriger

* Afficher les numéros de structure pour les fossés uniquement (fossés et fossés de parcellaire)

  * Propriétés > Etiquettes > Etiquettes basées sur des règles puis plus pour ajouter une règle
  * Dans Filtre, sélectionner le Constructeur d'expression et saisir :

  

  ```sql
  "type_fait" IN ('fossé parcellaire', 'fossé')	
  ```

  ou 

  ```sql
  "type_fait" LIKE 'fossé parcellaire'
  OR
  "type_fait" LIKE 'fossé'
  ```

  

  ![constructeur de chaîne d'expression](images/image_023.png)  

  

  * Etiqueter avec numpoly

## étape 12 : Effectuer des requêtes spatiales
:bulb: ​ [fiche technique : requête spatiale](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/05_requete_spatiale.html)

* Combien de faits sont coupés par des axes ? 
  * *Vecteur > Outils de recherche > Sélection par localisation*

 ![Sélection par localisation](images/image_025.png) 

* Quels vestiges ont été repérés au diagnostic (importez auparavant D107698_poly.shp) ?
* Sélectionner les faits du diag qui sont compris dans les zones prescrites et exportez-les (Exporter > Sauvegarder les entités sous > donnee_spatiale > vecteur georef > vecteur_analyse F111688_polyDiag.shp)



## étape 13 :  Effectuer des requêtes attributaires

:bulb: ​ [fiche technique : requête attributaire](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/04_requete_att.html)

* Combien de structures ont été totalement fouillées ?
* Combien de structures ont fait l’objet de relevés sur des minutes ?
* Combien de vestiges comportent de la terre cuite ?
* Combien de trous de poteau sont datés entre -2200 et -1600 ? 

Les requêtes se font à partir de la table attributaire, on peut passer 

* soit par l'outil de sélection par expression si l'on souhaite une sélection à l'issue de la question ![outil sélection par expression](images/image_026.png) 

* soit par le filtre avancé ![filtre avancé](images/image_027.png)



![filtre basé sur une expression](images/image_028.png) 



Combien de structures ont été totalement fouillées ?

```sql
 "_pourcent_" LIKE  '100%' 
```

Combien de structures ont fait l’objet de relevés sur des minutes ?

```sql
 "_num_min" IS NOT NULL
```

Combien de vestiges comportent de la terre cuite ?

```sql
 "_materiel" LIKE '%terre cuite%'
```

Combien de trous de poteau sont datés entre -2200 et -1600 ? 

```sql
 "_type_fait" LIKE 'TP' AND  ("datedebut" = '-2200' AND  "datefin" = -1600 )
```



# Obtenir des statistiques simples

## étape 14 :  Pour faire des statistiques et de la carto, obtenir un format de colonne adéquat

Après la jointure, les champs qui comportent des chiffres ont été stockés en format texte, il faut donc en modifier le format. La manipulation est la même que pour la mise à jour des champs suite à la [jointure attributaire](master/actu.md#étape-9-alimenter-le-sig-avec-les-inventaires-de-terrain-jointure-attributaire). 

* Créer un champ ‘‘larg’’ (réel ; 10 ; précision 2) avec les données de ‘‘_larg’’ 
* Créer un champ ‘‘poids’’ (entier ; 10) avec les données de ‘‘_poids’’ 



## étape 15 :  Obtenir des résultats statistiques 

* Quelle est la largeur moyenne des ‘foyers à pierres chauffées’ ? (couche F111688_poly_inv) 

  * Faire une sélection des foyers à pierres chauffées 

  ```sql
   "interpret" LIKE  'foyer a pierres chauffées' 
  ```

  

  * Outil statistique : ![outil statistique](images/image_029.png) 
  * Choisir la couche que l'on souhaite interroger : F111688_poly_inv
  * Choisir le champ qui contient l'information demandée : "_larg"
  * Ne pas oublier de cocher "Entités sélectionnées uniquement"

![statistiques](images/image_030.png) 



* Quel est le poids total de mobilier retrouvé dans des faits ? 
  * Couche : F111688_poly_inv
  * Champ : "poids"
* Quelles sont les altitudes min/max/moyenne de la fouille ? 
  * Couche : F111688_ptsTopos
  * Champ : "ALT"



# 6. Cartographier les données

## étape 16 :  Créer une carte de densité des isolats 

:bulb: [Fiche technique : Analyse par maille ou carte de densité](https://formationsig.gitlab.io/fiches-techniques/vecteur_donnees/06_analyse_maille.html)

* Création d'une grille : *Boite à outils de traitements > Création de vecteurs > Créer une grille*
  * Type de grille : rectangle (polygone)
  * Etendue de la grille : cliquer sur la boîte à trois points à droite et choisir Utiliser emprise de la couche F111688_emprise
  * Espacement horizontal : 10 mètres
  * Espacement vertical : 10 mètres
  * Après exécution, une couche temporaire "Grille" est créée

![algorithme créer une grille](images/image_031.png) 



* Compter le nombre de points par maille : *Vecteur > Outils d'analyse > Compter les points dans les polygones*
  * Polygones : Grille
  * Points : F111688_point
  * Champ de décompte : NB_POINTS
  * Après exécution, une couche temporaire "Compte" est créée

![outil compter les points dans les polygones](images/image_032.png) 

* Calculer la densité au m² des points (nb de points / surface du carré)

  * Dans la couche "Compte", calculatrice de champ : créer un champ "Freq", de type Nombre décimal, de longueur 5 et de précision 3

  ```sql
   "NB_POINTS"  / '100'
  ```

  * Nous avons divisé par 100 car la surface des carrés utilisés est de 100 m² (10 m x 10 m)

* Cartographier le résultat

  * *Propriétés > Symbologie > Gradué* avec la valeur "Freq"
  * Classer et utiliser le mode de discrétisation "Ruptures naturelles (Jenks)". Si besoin, mettre en blanc pour la valeur 0



## étape 17 :  Représenter la proportion de mobilier par fait 
**NB** : pour pouvoir récupérer la légende des valeurs liées au poids, il faut transformer la couche F111688_poly_inv en une couche de point

* *Vecteur > Outils de Géométrie > Centroïdes* 

  * Couche source :  F111688_poly_inv
  * Centroïdes : F111688_poly_pts 

  ![outil Centroïdes](images/image_033.png) 

* Sur F111688_poly_pts : *Propriétés > Symbologie > Symbole unique*

![Assistant taille](images/image_034.png) 

* *Symbole simple > Taille > Assistant Taille* > source : poids (Méthode : Surface)

![Outil cercles proportionnels](images/image_035.png) 

* *Rendu de couche > Contrôle de l’ordre de rendu des entités > Descendant*

![Contrôle de l'ordre de rendu des entités](images/image_036.png) 



* *Avancé > Légende définie par la taille des données > Taille manuelle des classes* 

![Légende des cercles proportionnels 1](images/image_037.png) 

![légende cercles proportionnels 2](images/image_038.png) 



## étape 18 :  Faire une Mise en page ‘‘Mobilier’’

:bulb: ​ [fiche technique : créer une carte simple](https://formationsig.gitlab.io/fiches-techniques/miseenpage/01_mise_en_page_base.html) 

![Mise en page mobilier](images/image_039.png) 

* Créer une nouvelle Mise en page (Menu Projet > Nouvelle Mise en page) et lui donner le titre "Mobilier"
* Format Paysage 170 mm x 130 mm : clic D dans la page blanche > Propriétés de la page
* Ajouter la carte 1 : Menu Ajouter un élément > Ajouter carte
  * Échelle : 2000
  * Taille : 90 mm de largeur, 130 mm de hauteur
  * Couches : Verrouiller les couches / Verrouiller le style des couches
  * Grilles : ajouter une grille
    * Sous-menu Apparence
      * Type de grille : Croix
      * Intervalle : 100 m en X et en Y
      * Décalage de 20 en X et de 40 en Y
      * Largeur de croix : 1.5 mm
      * Style de ligne : largeur 0.15
    * Sous-menu Cadre 
      * Style de cadre : marqueurs à l'intérieur
    * Sous-menu Afficher les coordonnées : cocher pour activer
      * Gauche : Tout afficher / Dans un cadre / Ascendant vertical
      * Droite : Désactivé
      * Haut : Désactivé
      * Bas : Tout afficher / Dans un cadre / Horizontal
      * Police : Arial, 6 pt
      * Précision des coordonnées : 0 
  * Ajouter un cadre, épaisseur 0.15 
* Ajouter un nord : Menu Ajouter un élément > Ajouter Flèche du nord
  * On peut choisir son propre nord en svg : à droite de Source de l'image, cliquer sur la boîte à trois points et indiquer l'emplacement
  * On peut aussi utiliser les quelques nord disponibles dans le sous-menu Rechercher dans les répertoires
* Ajouter une échelle : Menu Ajouter un élément > Ajouter Echelle graphique
  * Carte : Carte 1
  * Style : Boîte unique
  * Segments
    *  droite 5
    * Largeur fixe : 20 unités (soit 20 mètres)
    * Hauteur : 1 mm
  * Affichage 
    * Marge de l'étiquette : 1 mm
    * Largeur de ligne : 0.15
  * Polices et couleurs : Arial, 6 pt



Ajouter une 2e carte pour représenter les densités

* Taille : 70 x 60 mm, échelle 2000e, verrouiller les couches / verrouiller le style de couches
* Ajouter un nord et une échelle graphique simple
* Ajouter une légende
  * Décocher mise à jour auto
  * Cocher *Only show items inside linked map* pour ne montrer que les éléments réellement présents sur l'emprise de la carte 
  * Renommer les couches avec des noms plus clairs (double clic sur l'élément)
  * Cacher le groupe vecteurs_ope
  * Régler la taille des symboles (largeur 4 mm, hauteur 2.5 mm) et leur espacement (espace entre les symboles 0.5 mm). Supprimer l'espace en-dessous du titre de légende
* Ajouter une étiquette (texte libre) : Densité des isolats (Jenks)



Ajouter une 3e carte 

* Taille : 70 x 60 mm, échelle 4000e, afficher les vestiges en gris foncé
* Ajouter un nord et une échelle graphique simple
* Ajouter un aperçu pour la carte 1 
  * *Propriétés de la carte > Aperçu > Bouton Plus > Cadre de carte* : Carte 1
  * Style de cadre : pas de remplissage, trait rouge, largeur 0.46 mm. Attention à bien cocher Style de trait : ligne continue (par défaut, il n'y a pas de ligne)
* Ajouter un aperçu pour la carte 2
  * *Propriétés de la carte > Apercu > Bouton Plus > Cadre de carte* : Carte 2
  * Style de cadre : pas de remplissage, trait orange, largeur 0.46 mm
* Ajouter une ligne entre chaque aperçu et sa carte correspondante
  * Menu *Ajouter un élément > Ajouter flèche*
  * Propriétés principales : ligne simple, couleur rouge ou orange, largeur 0.46 mm
  * Symbole de début : aucun
  * Symbole de fin : aucun



Bravo, vous avez fini !