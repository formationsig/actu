# ![actu](images/sigActu.png) Actualisation des Connaissances

* durée: 1 journée

* pré-requis : niveau 1 découverte

* objectifs:

  * Révision des principes et des fonctionnalités incontournables du SIG

    

Reprendre tous les éléments abordés lors de la formation SIG Niveau 1 (Module découverte)

1. Paramétrage général du projet
2. Alimentation du SIG : ajout de données
3. Numérisation et accrochage
4. Manipulation de données attributaires
5. Représentation & interrogation de données
6. Édition de cartes



## Supports de formation

* Ce support en ligne: https://formationsig.gitlab.io/actu
* Le [diaporama](https://slides.com/archeomatic/revision_niv_1)
* La version PDF à jour  [![icon_pdf](images/pdf.png)](https://gitlab.com/formationsig/actu/-/jobs/artifacts/master/raw/public/actu.pdf?job=pages)

## Ressources et documentation

* Des **fiches techniques** peuvent  être utiles, vous les trouverez à l'adresse  https://formationsig.gitlab.io/fiches-techniques



[ :leftwards_arrow_with_hook: Accueil - Formation SIG](https://formationsig.gitlab.io/toc/)



Toute cette documentation est sous licence sous licence [![CC](images/CC_BY_ND.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)